#!/usr/bin/env bash

# Depurar DEBUG=1
DEBUG=1

# Kill polybar
killall -q polybar

# Launch bar1 and bar2

polybar main --config=~/.config/polybar/config.ini &
#polybar bottom  --config=~/.config/polybar/config.ini &

echo "Bars launched..."

