function on_pause_change(name, value)
    if value == true then
        mp.set_property("pause", "yes")
    end
end
mp.observe_property("charging", "bool", on_pause_change)

